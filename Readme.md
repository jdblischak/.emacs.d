# Usage directions

If you currently have your own .emacs.d directory, change its name to try out
this one.

	mv .emacs.d .emacs.d.orig

Clone the Bitbucket repo.

	git clone git@bitbucket.org:jdblischak/.emacs.d.git

Update the submodules.

	git submodule init # Adds .gitmodules info to .git/config
	git submodule update
	
# Installation record

## [ESS][]: Emacs Speaks Statistics

Cloned from GitHub [repo][ess-repo].

	git submodule add git@github.com:emacs-ess/ESS.git elisp/ESS
	cd elisp/ESS
	git checkout v13.05

## [Markdown Mode][md]

Cloned git [repo][md-repo].

	git submodule add git://jblevins.org/git/markdown-mode.git elisp/markdown-mode
	cd elisp/markdown-mode
	git checkout stable

## [python-mode][py]

Cloned from GitHub [repo][py-repo].

	git submodule add git@github.com:emacsmirror/python-mode.git elisp/python-mode
	cd elisp/python-mode
	git checkout 6.1.1

## [Pymacs][]

Cloned from GitHub [repo][pymacs-repo].

	git submodule add git@github.com:pinard/Pymacs.git elisp/Pymacs
	cd elisp/Pymacs
	git checkout v0.23
	make install

[ess]: http://ess.r-project.org/Manual/ess.html#Latest-version
[ess-repo]: https://github.com/emacs-ess/ESS
[md]: http://jblevins.org/projects/markdown-mode/
[md-repo]: http://jblevins.org/git/markdown-mode.git
[py]: https://launchpad.net/python-mode/trunk/6.0.12/
[py-repo]: https://github.com/emacsmirror/python-mode

[pymacs]: http://pymacs.progiciels-bpi.ca/pymacs.html
[pymacs-repo]: https://github.com/pinard/Pymacs
