;;; Adding .emacs.d/elisp directories to path
;;; http://www.emacswiki.org/emacs/LoadPath#AddSubDirectories
(let ((default-directory "~/.emacs.d/elisp/"))
      (normal-top-level-add-to-load-path '("ESS/lisp/" "markdown-mode" "python-mode" "Pymacs")))

;;; ESS: Emacs Speaks Statistics
(require 'ess-site)
(defun myindent-ess-hook ()
  (setq ess-indent-level 2))
(add-hook 'ess-mode-hook 'myindent-ess-hook)

;;; markdown-mode
(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))

;;; python-mode
(setq py-install-directory "~/.emacs.d/elisp/python-mode/")
(require 'python-mode)

;;; unload python.el commands (recommended)
(when (featurep 'python) (unload-feature 'python t))

;;; use IPython
(setq-default py-shell-name "ipython")
(setq py-force-py-shell-name-p t)

;;; Pymacs
(autoload 'pymacs-apply "pymacs")
(autoload 'pymacs-call "pymacs")
(autoload 'pymacs-eval "pymacs" nil t)
(autoload 'pymacs-exec "pymacs" nil t)
(autoload 'pymacs-load "pymacs" nil t)
(autoload 'pymacs-autoload "pymacs")

;;; Add column numbers
(setq column-number-mode t)

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(delete-auto-save-files t)
 '(fill-column 80)
 '(inhibit-startup-screen t)
 '(make-backup-files nil)
 '(rename-auto-save-file t))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
